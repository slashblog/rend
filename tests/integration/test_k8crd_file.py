import os

import pytest
import yaml

import rend.exc


def test_k8_crd(mock_hub, hub, FDIR):
    fn = os.path.join(FDIR, "k8_crd.sls")
    data = yaml.safe_load(open(fn))
    mock_hub.rend.k8crd.render = hub.rend.k8crd.render
    ret = mock_hub.rend.k8crd.render(data)
    assert ret
    assert isinstance(ret, dict)
    assert len(ret) == 1
    assert "new-rg" in ret
    assert isinstance(ret["new-rg"], dict)
    assert len(ret["new-rg"]) == 1
    assert "azure.resource_management.resource_groups.present" in ret["new-rg"]
    assert isinstance(
        ret["new-rg"]["azure.resource_management.resource_groups.present"], list
    )
    assert len(ret["new-rg"]["azure.resource_management.resource_groups.present"]) == 2


def test_invalid_type(mock_hub, hub):
    mock_hub.rend.k8crd.render = hub.rend.k8crd.render
    with pytest.raises(rend.exc.RenderException) as exc:
        mock_hub.rend.k8crd.render("")
    assert exc.value.args[0] == "Invalid data type: dict was expected"


def test_missing_attributes(mock_hub, hub):
    mock_hub.rend.k8crd.render = hub.rend.k8crd.render

    data = {"apiVersion": "", "kind": "", "metadata": {}}

    for key in ["apiVersion", "kind", "metadata"]:
        value = data.pop(key)
        with pytest.raises(rend.exc.RenderException) as exc:
            mock_hub.rend.k8crd.render(data)
        assert exc.value.args[0] == f"Render error: `{key}` is a mandatory attribute"

        data[key] = 0
        with pytest.raises(rend.exc.RenderException) as exc:
            mock_hub.rend.k8crd.render(data)
        assert (
            exc.value.args[0]
            == f"Render error: {key} type error: {type(value)} expected"
        )

        data[key] = value

    with pytest.raises(rend.exc.RenderException) as exc:
        mock_hub.rend.k8crd.render(data)
    assert exc.value.args[0] == f"Render error: Proving a name in metadata is mandatory"

    data["metadata"] = {"name": ""}
    with pytest.raises(rend.exc.RenderException) as exc:
        mock_hub.rend.k8crd.render(data)
    assert exc.value.args[0] == f"Render error: apiVersion string is invalid"
